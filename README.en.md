# 贪吃蛇项目

#### Description
阶段性目标：
1：界面优化，为游戏添加一个倒计时功能，当倒计时结束游戏就结束。
2：游戏中会随机出现有毒的食物，若蛇吃到有毒食物分数会减少，若一段时间有毒食物未被吃掉则自动消失。
3：当GAMEOVER时，会查看当前分数是否高于历史最高分，若高于则要求留下玩家信息保存到本地。
阶段一：起始时间2019/4/8
终止时间2019/5/10
阶段二：起始时间2019/5/10
终止时间2019/6/10
阶段三：起始时间2019/6/10
终止时间2019/7/10

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
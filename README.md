# 贪吃蛇项目

#### 介绍
阶段性目标：
1：界面优化，为游戏添加一个倒计时功能，当倒计时结束游戏就结束。
2：游戏中会随机出现有毒的食物，若蛇吃到有毒食物分数会减少，若一段时间有毒食物未被吃掉则自动消失。
3：当GAMEOVER时，会查看当前分数是否高于历史最高分，若高于则要求留下玩家信息保存到本地。
阶段一：起始时间2019/4/8
终止时间2019/5/10
阶段二：起始时间2019/5/10
终止时间2019/6/10
阶段三：起始时间2019/6/10
终止时间2019/7/10

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)